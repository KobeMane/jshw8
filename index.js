//дом это интерфейс, который дает доступ к элементам хтмл на странице
//хтмл выводит в консоль текст с тегами, а текст выводит только текст без тегов, + текст особо не используют
//ойй, ну обратиться можно большим количеством способов: документ. создать элемент, найти по селектору все, либо же первый, по айди, по тегу, по атрибубу. Какой лучше? ну по айдишке более принято, наверное. а так, если выбирать из селекторов, то квери олл.

const paragraph = document.querySelectorAll("p").forEach((paragraph) => {
  paragraph.style.background = "red";
});

const list = document.getElementById("optionsList");
console.log(list);
console.log(list.parentElement);
console.log(list.children);
const test = document.getElementById("testParagraph");
test.textContent = "This is a paragraph";

const content = document.querySelector(".main-header").children;
for (const element of content) {
    element.className = "nav-item";
};
console.log(content);

const delElemClass = document.querySelectorAll(".section-title");
for (const el of delElemClass) {
    el.classList.remove("section-title");
}
console.log(delElemClass);
